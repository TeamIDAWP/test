package ar.com.call.center.dispacher.core.test;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.ExecutionException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ar.com.call.center.dispacher.core.exceptions.DispacherException;
import ar.com.call.center.dispacher.core.exceptions.OperatorInitException;
import ar.com.call.center.dispacher.services.Services;
import ar.com.call.center.dispacher.services.dtos.CallDTO;
import ar.com.call.center.spring.context.SpringContext;

@Component
public class DispacherTest {
	
	
	@Autowired
	protected Services services;	
	
	@Autowired
	protected ReceptorCallTest receptorTest;
	
	@Before
	public void loadReceptorsCall(){
		System.out.println("*** Inicialized test unit ***");
		try {
			this.getReceptorTest().FiveReceptorsInMemory();
		} catch (DispacherException | OperatorInitException e) { 
			e.printStackTrace();
		}
	}
	

	/***
	 * Test: carga 5 operadores
	 * Test: carga 10 llamadas simultaneas
	 * @param batchSize
	 * @throws OperatorInitException 
	 * @throws DispacherException 
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@Test
	public void testTenCallsByFiveOperators() throws DispacherException, OperatorInitException, InterruptedException, ExecutionException {
		System.out.println("****** BEGIN TEST UNIT FIVE OPERATORS BY TEN CALLS ****");		
		
		for (CallDTO call : SimulateCallsTest.getCalls()) {
			try{
				this.getServices().dispachCall(call);			
			}catch(DispacherException e){
				assertTrue("can't take call test", false);
			}
		}
		
		assertTrue("take 10 call test", true);
	}

	public ReceptorCallTest getReceptorTest() {
		this.receptorTest = (ReceptorCallTest)SpringContext.getContext().getBean(ReceptorCallTest.class);
		return receptorTest;
	}

	public void setReceptorTest(ReceptorCallTest receptorTest) {
		this.receptorTest = receptorTest;
	}


	public Services getServices() {
		this.services = (Services)SpringContext.getContext().getBean(Services.class);
		return this.services;
	}


	public void setServices(Services services) {
		this.services = services;
	}
}
