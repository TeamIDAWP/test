package ar.com.call.center.dispacher.core.test;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.ExecutionException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ar.com.call.center.dispacher.core.Dispacher;
import ar.com.call.center.dispacher.core.ReceiveCall;
import ar.com.call.center.dispacher.core.exceptions.DispacherException;
import ar.com.call.center.dispacher.core.exceptions.OperatorInitException;
import ar.com.call.center.dispacher.services.dtos.CallDTO;
import ar.com.call.center.spring.context.SpringContext;

@Component
public class DispacherExtrasTest {
	
	@Autowired
	protected Dispacher dispacher;
	
	@Autowired
	protected ReceptorCallTest receptorTest;
	
	@Before
	public void loadReceptorsCall(){
		System.out.println("*** Inicialized test unit ***");
		try {
			this.getReceptorTest().twelveReceptorsInMemory();
		} catch (DispacherException | OperatorInitException e) {
			e.printStackTrace();
		} 
		
	}
	
	/***
	 * Test: carga 10 operadores
	 * Test: carga 20 llamadas simultaneas
	 * @param batchSize
	 * @throws OperatorInitException 
	 * @throws DispacherException 
	 */
	@Test
	public void testTwentyCallsByTenOperators() throws DispacherException, OperatorInitException {
		System.out.println("****** BEGIN TEST UNIT TEN OPERATORS BY TWENTY CALLS ****");
		
		
		for (CallDTO call : SimulateCallsTest.getTwentyCalls()) {
			try{
				ReceiveCall receiveCall = new ReceiveCall(call.getCallIdentity());
				this.getDispacher().dispachCall(receiveCall);			
			}catch(DispacherException | InterruptedException | ExecutionException e){
				assertTrue("can't take call test", false);
			}
		}
		
		this.getReceptorTest().clearOperators();
		assertTrue("take 20 call test", true);
	}				

	public Dispacher getDispacher() {
		this.dispacher = (Dispacher)SpringContext.getContext().getBean(Dispacher.class);
		return dispacher;
	}

	public void setDispacher(Dispacher dispacher) {
		this.dispacher = dispacher;
	}

	public ReceptorCallTest getReceptorTest() {
		this.receptorTest = (ReceptorCallTest)SpringContext.getContext().getBean(ReceptorCallTest.class);
		return receptorTest;
	}

	public void setReceptorTest(ReceptorCallTest receptorTest) {
		this.receptorTest = receptorTest;
	}
}
