package ar.com.call.center.dispacher.core.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ar.com.call.center.dispacher.core.enums.ReceptorType;
import ar.com.call.center.dispacher.core.exceptions.DispacherException;
import ar.com.call.center.dispacher.core.exceptions.OperatorInitException;
import ar.com.call.center.dispacher.services.Services;
import ar.com.call.center.dispacher.services.dtos.ReceptorDTO;

@Component
public class ReceptorCallTest {
	
	@Autowired
	private Services services; 
	
	public void clearOperators(){
		this.getServices().clearReceptors();
	}
	
	public void twelveReceptorsInMemory() throws DispacherException, OperatorInitException{		
		
		this.getServices().registerReceptor(new ReceptorDTO("estela_suarez", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("juan_lopez", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("maria_benitez", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("julio_silva", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("rosa_mosqueta", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("enzo_perez", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("juana_chilirs", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("alberto_corea", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("sofia_galan", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("ernesto_sabato", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("analia_galan", ReceptorType.OPERATOR));
		
		this.getServices().registerReceptor(new ReceptorDTO("marisa_estevez", ReceptorType.SUPERVISOR));
		this.getServices().registerReceptor(new ReceptorDTO("mauro_diaz", ReceptorType.DIRECTOR));
		
	}
	
	
	public void FiveReceptorsInMemory() throws DispacherException, OperatorInitException{
				
		this.getServices().registerReceptor(new ReceptorDTO("estela_suarez", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("juan_lopez", ReceptorType.OPERATOR));
		this.getServices().registerReceptor(new ReceptorDTO("maria_benitez", ReceptorType.OPERATOR));		
		
		this.getServices().registerReceptor(new ReceptorDTO("marisa_estevez", ReceptorType.SUPERVISOR));
		this.getServices().registerReceptor(new ReceptorDTO("mauro_diaz", ReceptorType.DIRECTOR));
		
	}

	public Services getServices() {		
		return new Services();
	}

	public void setServices(Services services) {
		this.services = services;
	}	
}
