package ar.com.call.center.dispacher.core.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({DispacherTest.class, DispacherExtrasTest.class})
public class BasicTestSuite {

}
