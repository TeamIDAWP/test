package main;

import java.util.concurrent.atomic.AtomicLong;

import ar.com.call.center.dispacher.core.exceptions.DispacherException;
import ar.com.call.center.dispacher.core.exceptions.OperatorInitException;
import ar.com.call.center.dispacher.services.Services;
import ar.com.call.center.dispacher.services.dtos.CallDTO;
import ar.com.call.center.test.calls.SimulateCalls;
import ar.com.call.center.test.calls.SimulateReceptors;

public class MainCallCenter {

	private static Services services = new Services();
	
	public static void main(String[] args) throws DispacherException, OperatorInitException {
		System.out.println("**** LOAD RECEPTORS IN MEMORY ****");
		SimulateReceptors.loadReceptors();
		
		System.out.println("**** SIMULATE 20 CALLS CONCURRENTS ****");
		for (CallDTO call : SimulateCalls.getCalls()) {
			try{
				services.dispachCall(call);			
			}catch(DispacherException e){
				e.printStackTrace();
			}
		}

	}
	

}
