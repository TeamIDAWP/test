package ar.com.call.center.dispacher.core.operators;

import ar.com.call.center.dispacher.core.exceptions.OperatorInitException;

public class Supervisor extends ReceptorCall{

	public Supervisor(String username, Integer priority) throws OperatorInitException{
		super(username, priority);
	}
}
