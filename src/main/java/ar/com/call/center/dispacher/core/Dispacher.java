package ar.com.call.center.dispacher.core;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ar.com.call.center.dispacher.core.exceptions.DispacherException;
import ar.com.call.center.dispacher.core.operators.ReceptorCall;

@Service
public class Dispacher {

	private static final Logger LOGGER = LoggerFactory.getLogger(Dispacher.class);
	
	
	private ExecutorService executor;
	private Set<Call> inprogress;
	private Set<Call> inwait;
	private BlockingQueue<ReceptorCall> receptoresCall; 
	
	@Value("${call.pollCallSize}")
	private int pollCallSize;
	
	@Value("${call.operator.timeout}")
	private long operatorTimeout;
	
	@PostConstruct
	public void inizializer(){
		//Inicializamos el Set para trabajar en modo concurrente 
		this.inprogress = Collections.synchronizedSet(new HashSet<>(this.pollCallSize));
		
		//Inicializamos la collection para las llamadas que quedan en espera
		this.inwait = Collections.synchronizedSet(new HashSet<>());
		
		this.receptoresCall = new PriorityBlockingQueue<ReceptorCall>();
		
		//Inicializamos el pool de hilos con el que vamos a trabajar
		this.executor = Executors.newFixedThreadPool(this.pollCallSize, new ThreadFactory() {			
			@Override
			public Thread newThread(Runnable r) {
				Thread thread = new Thread(r);
				thread.setName("thread-call");
				return thread;
			}
		});
	}
	
	public Call dispachCall(Call call) throws DispacherException, InterruptedException, ExecutionException{
		LOGGER.info("Dispaching call: " + call.getCallIdentify());
		try{
		final Future<Call> list = (Future<Call>) this.executor.submit(new Callable<Call>() {
			
			@Override
			public Call call() throws Exception{
				takeCall(call);
				return call;
			}
		});
		}catch(Exception e){
			e.printStackTrace();
			throw new DispacherException("test");
		}
		
		return call;				
	}
	
	/***
	 * Obtenemos una llamada y la asignamos a un receptor disponible
	 * 
	 * EXTRA PLUS
	 * Si la llamada no puede ser atendida por un operador disponible, enviamos la llamada a una lista de espera.
	 * Cuando el sistema libera un operador de un llamado, transfiere la llamada en espera a una llamada en proceso
	 * @param call
	 * @throws DispacherException
	 */
	private void takeCall(final Call call) throws DispacherException{
		ReceptorCall receptor = null;
		// registramos la llamada en la lista
		this.inprogress.add(call);
		
		try{
			receptor = obtainReceptorCall(call);
		}catch(DispacherException ex){
			//EXTRA PLUS 
			this.inwait.add(call); //cargamos la llamada en lista de espera
			LOGGER.info("operators busy, load call in list wait");
			throw new DispacherException("can't obtain a operator, load call in list wait");
		}
		
		LOGGER.debug("Taking operator: " + receptor.getUsername());
		try{
			call.run(this, receptor);
		}catch(InterruptedException e){
			
			this.inprogress.remove(call);						
			LOGGER.error("can't run call");
			throw new DispacherException("can't run call");
		}
	}
	
	
	public void registerReceptorCall(ReceptorCall receptorCall) throws DispacherException{
		if(receptorCall==null) throw new DispacherException("Operator not register");
		if(!this.receptoresCall.contains(receptorCall)){
			this.receptoresCall.offer(receptorCall);
		}else{
			throw new DispacherException("Operator already register");
		}
	}
	
	public void unregisterReceptorCall(ReceptorCall receptorCall) throws DispacherException{
		if(receptorCall == null) throw new DispacherException("Operator not register actually");
		if(!this.receptoresCall.contains(receptorCall)) throw new DispacherException("Operator not register");
		this.receptoresCall.remove(receptorCall);
	}
	
	public void clearReceptors(){
		this.receptoresCall.clear();
	}
	
	public void endCall(final Call call){
		try{
			registerReceptorCall(call.getReceptorCall());
			this.processVerifyListWait();
		}catch(DispacherException e){
			LOGGER.debug("can't retun operator: " + e.getMessage());
		}
	}
	
	/***
	 * 1) Verifico si la lista de espera tiene llamadas pendientes
	 * 2) Si es las tiene, tomo la primera y la paso a lista de llamadas en progreso para continuear su circuito
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 * @throws DispacherException 
	 */
	private void processVerifyListWait(){
		LOGGER.info("number of calls in wait: " + this.inwait.size());
		if(this.inwait.size()>0){
			Iterator it= (Iterator) this.inwait.iterator();
			while(it.hasNext()){
				LOGGER.debug("deriving call waiting ...");		
				Call call = (Call)it.next();
				try {
					this.dispachCall(call);
					this.inwait.remove(call);
				} catch (DispacherException | InterruptedException
						| ExecutionException e) {
					e.printStackTrace();
					LOGGER.debug("can't take call, ocurred an error");
				}
				
			}						
		}
	}
	
	/***
	 * Obtenemos un receptor para el llamado, de receptores cargados en memoria
	 * Este m�todo podr�a comunicarse con un DAO, que obtenga los receptor de un motor de base de datos
	 * @param call
	 * @return
	 * @throws DispacherException
	 */
	private ReceptorCall obtainReceptorCall(final Call call) throws DispacherException{
		ReceptorCall receptor = null;
		
		try{
			receptor = this.receptoresCall.poll(operatorTimeout, TimeUnit.MILLISECONDS); 
		}catch(InterruptedException e){
			removeInProgress(call);
		}
				
		if(receptor==null){
			removeInProgress(call);
		}
		
		return receptor;
	}
	
	private void removeInProgress(final Call call) throws DispacherException{
		this.inprogress.remove(call);
		throw new DispacherException("can't obtain receptor call");		
	}
	
}
