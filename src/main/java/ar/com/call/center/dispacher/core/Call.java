package ar.com.call.center.dispacher.core;

import ar.com.call.center.dispacher.core.enums.CallStatus;
import ar.com.call.center.dispacher.core.operators.ReceptorCall;

public interface Call {

	 ReceptorCall getReceptorCall();	 
	 Long getCallIdentify();
	 CallStatus getStatus();
	 
	 void run(Dispacher dispacher, ReceptorCall receptor) throws InterruptedException;
	 
}
