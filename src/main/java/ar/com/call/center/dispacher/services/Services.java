package ar.com.call.center.dispacher.services;

import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ar.com.call.center.dispacher.core.Dispacher;
import ar.com.call.center.dispacher.core.ReceiveCall;
import ar.com.call.center.dispacher.core.exceptions.DispacherException;
import ar.com.call.center.dispacher.core.exceptions.OperatorInitException;
import ar.com.call.center.dispacher.core.operators.ReceptorCall;
import ar.com.call.center.dispacher.services.dtos.CallDTO;
import ar.com.call.center.dispacher.services.dtos.ReceptorDTO;
import ar.com.call.center.spring.context.SpringContext;

/***
 * Class: esta clase podr�a transformarse en una capa de servicios 
 * @author Gabriel
 *
 */
@Component
public class Services{

	public static final Logger LOGGER = LoggerFactory.getLogger(Services.class);
	
	@Autowired
	private Dispacher dispacher;
	
	@Autowired
	private ReceptorsFactory receptorsFactory;
	
	/***
	 * M�todo para registrar operadores
	 * @param receptor
	 * @throws DispacherException
	 * @throws OperatorInitException
	 */
	public void registerReceptor(ReceptorDTO receptor) throws DispacherException, OperatorInitException{
		
		ReceptorCall receptorCall = null;
		try{
			receptorCall = this.getReceptorsFactory().getReceptorCall(receptor);
			this.getDispacher().registerReceptorCall(receptorCall);
			LOGGER.debug("Cargando " + receptor.getType() + ": " + receptorCall.getUsername());
		}catch(DispacherException d){
			throw new DispacherException(d.getMessage());
		}catch(OperatorInitException e){
			throw new OperatorInitException(e.getMessage());
		}
		
	}
	
	public void unregisterReceptor(ReceptorDTO receptor) throws DispacherException, OperatorInitException{
		try{
			this.getDispacher().unregisterReceptorCall(new ReceptorCall(receptor.getUsername(), receptor.getType().getPriority()));
		}catch(DispacherException e){
			throw new DispacherException(e.getMessage());
		}catch(OperatorInitException i){
			throw new OperatorInitException(i.getMessage());
		}
	}	
	
	public void clearReceptors(){
		this.getDispacher().clearReceptors();
	}
	
	
	/***
	 * M�todo para iniciar una llamada al Call Center
	 * @param call
	 * @throws DispacherException
	 */
	public void dispachCall(CallDTO call) throws DispacherException{
		ReceiveCall receiveCall = new ReceiveCall(call.getCallIdentity());
		try{
			this.getDispacher().dispachCall(receiveCall);
		}catch(DispacherException | InterruptedException | ExecutionException e){
			throw new DispacherException(e.getMessage());
		}
	}

	public Dispacher getDispacher() {
		this.dispacher = (Dispacher) SpringContext.getContext().getBean("dispacher");
		return dispacher;
	}

	public void setDispacher(Dispacher dispacher) {
		this.dispacher = dispacher;
	}

	public ReceptorsFactory getReceptorsFactory() {
		this.receptorsFactory = (ReceptorsFactory) SpringContext.getContext().getBean("receptorsFactory");
		return receptorsFactory;
	}

	public void setReceptorsFactory(ReceptorsFactory receptorsFactory) {
		this.receptorsFactory = receptorsFactory;
	}
		
}