package ar.com.call.center.dispacher.core;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ar.com.call.center.dispacher.core.enums.CallStatus;
import ar.com.call.center.dispacher.core.operators.ReceptorCall;

public class ReceiveCall implements Call{

	private static final Logger LOGGER = LoggerFactory.getLogger(ReceiveCall.class);
	
	/***
	 * Los rangos de duraci�n establecidos en el documento del ejercicio
	 * indicaban una duraci�n aletoria de entre 5 y 10 segundos
	 */
	private static final int MIN_DURATION = 5;
	private static final int MAX_DURATION = 10;
	private Integer durationRamdom;
	
	private ReceptorCall receptorCall;
	private Long callIdentity;
	private CallStatus status;
	
	public ReceiveCall(Long identity){
		this.status = CallStatus.INCOMING;
		this.callIdentity = identity;
		this.durationRamdom = (MIN_DURATION + (new Random().nextInt(MAX_DURATION - MIN_DURATION + 1))) * 1000;
	}
	
	@Override
	public void run(Dispacher dispacher, ReceptorCall receptor)
			throws InterruptedException {
		
		LOGGER.debug("Running call Operator - " + receptor.getUsername() + " - Thread: " + Thread.currentThread().getName() + " - Random Duration: " + this.getDurationRamdom());		
		this.receptorCall = receptor;
		this.status = CallStatus.INPROGRESS;
		
		Thread.sleep(this.getDurationRamdom());
		
		this.status = CallStatus.FINISHED;
		LOGGER.debug("End call Operator - " + receptor.getUsername() + " - Thread: " + Thread.currentThread().getName() + " - Random Duration: " + this.getDurationRamdom());
		dispacher.endCall(this);
		
	}
	
	
	@Override
	public ReceptorCall getReceptorCall() {
		return this.receptorCall;
	}

	@Override
	public Long getCallIdentify() {
		return this.callIdentity;
	}

	@Override
	public CallStatus getStatus() {
		return this.status;
	}

	public Integer getDurationRamdom() {
		return durationRamdom;
	}

	public void setDurationRamdom(Integer durationRamdom) {
		this.durationRamdom = durationRamdom;
	}

	public Long getCallIdentity() {
		return callIdentity;
	}

	public void setCallIdentity(Long callIdentity) {
		this.callIdentity = callIdentity;
	}

	public void setReceptorCall(ReceptorCall receptorCall) {
		this.receptorCall = receptorCall;
	}

	public void setStatus(CallStatus status) {
		this.status = status;
	}
	
	public int hashCode(){
		return this.getCallIdentify().intValue();
	}


}
