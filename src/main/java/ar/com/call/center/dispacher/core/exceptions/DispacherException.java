package ar.com.call.center.dispacher.core.exceptions;

public class DispacherException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public DispacherException(String msg){
		super(msg);
	}

}
