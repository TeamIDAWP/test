package ar.com.call.center.dispacher.services;

import org.springframework.stereotype.Component;

import ar.com.call.center.dispacher.core.enums.ReceptorType;
import ar.com.call.center.dispacher.core.exceptions.OperatorInitException;
import ar.com.call.center.dispacher.core.operators.Director;
import ar.com.call.center.dispacher.core.operators.Operator;
import ar.com.call.center.dispacher.core.operators.ReceptorCall;
import ar.com.call.center.dispacher.core.operators.Supervisor;
import ar.com.call.center.dispacher.services.dtos.ReceptorDTO;

@Component
public class ReceptorsFactory {

	/***
	 * Patron de dise�o: Factory Method
	 * @param receptor
	 * @return
	 * @throws OperatorInitException
	 */
	public ReceptorCall getReceptorCall(ReceptorDTO receptor) throws OperatorInitException{
		
		ReceptorCall receptorCall = null;
		
		try{
			if(receptor.getType().equals(ReceptorType.DIRECTOR)){
				receptorCall = new Director(receptor.getUsername(), ReceptorType.DIRECTOR.getPriority());
			}else if(receptor.getType().equals(ReceptorType.SUPERVISOR)){
				receptorCall = new Supervisor(receptor.getUsername(), ReceptorType.SUPERVISOR.getPriority());
			}else{
				receptorCall = new Operator(receptor.getUsername(), ReceptorType.OPERATOR.getPriority());
			}
		}catch(Exception e){
			throw new OperatorInitException("Operator can't inizializer"); 
		}
		
		
		return receptorCall;
	} 
}
