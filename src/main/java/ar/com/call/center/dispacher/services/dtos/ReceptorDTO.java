package ar.com.call.center.dispacher.services.dtos;

import ar.com.call.center.dispacher.core.enums.ReceptorType;

/***
 * DTO: Data Transfer Object
 * @author Gabriel
 *
 */
public class ReceptorDTO {
	
	public ReceptorDTO(String username, ReceptorType type){
		this.username = username;
		this.type = type;
	}
	
	private String username;
	private ReceptorType type;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public ReceptorType getType() {
		return type;
	}
	public void setType(ReceptorType type) {
		this.type = type;
	}
	
}
