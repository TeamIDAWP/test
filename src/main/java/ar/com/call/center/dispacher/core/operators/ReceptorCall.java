package ar.com.call.center.dispacher.core.operators;

import ar.com.call.center.dispacher.core.exceptions.OperatorInitException;

public class ReceptorCall implements Comparable<ReceptorCall> {

	private Integer priority;
	private long numberAsignation;
	private String username;
	
	public ReceptorCall(String username, Integer priority) throws OperatorInitException{		
		if(username==null){
			throw new OperatorInitException("User must be defined");
		}
		this.priority = (priority==null) ? 1 : priority;
		this.username = username;
	}	
	
	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public long getNumberAsignation() {
		return numberAsignation;
	}

	public void setNumberAsignation(long numberAsignation) {
		this.numberAsignation = numberAsignation;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * 1) Realizo una comparaci�n por Prioridad en el tipo de receptor de la llamada
	 * 2) posteriormente por n�mero de asignaci�n
	 */
    public int compareTo(ReceptorCall o){    
    	int result = o == null ? 1 : this.getPriority().compareTo(o.getPriority());
    	if(result==0 && o!=this){
    		result = (this.getNumberAsignation()<o.getNumberAsignation()) ? -1 : 1;
    	}
    	return result;
    }
}
