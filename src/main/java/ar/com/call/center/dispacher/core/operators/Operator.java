package ar.com.call.center.dispacher.core.operators;

import ar.com.call.center.dispacher.core.exceptions.OperatorInitException;

public class Operator extends ReceptorCall{

	public Operator(String username, Integer priority) throws OperatorInitException{
		super(username, priority);
	}
	
}