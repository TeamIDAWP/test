package ar.com.call.center.dispacher.core.enums;

public enum ReceptorType{
	OPERATOR(1), SUPERVISOR(2), DIRECTOR(3);
	
	private int priority;
	
	ReceptorType(int prioridad){
		this.priority = prioridad;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
