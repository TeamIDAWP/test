package ar.com.call.center.dispacher.services.dtos;

public class CallDTO {

	private long callIdentity;
	
	public CallDTO(long identity){
		this.callIdentity = identity;
	}

	public long getCallIdentity() {
		return callIdentity;
	}

	public void setCallIdentity(long callIdentity) {
		this.callIdentity = callIdentity;
	}
}
