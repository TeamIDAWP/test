package ar.com.call.center.dispacher.core.exceptions;

public class OperatorInitException extends Exception{

	private static final long serialVersionUID = 1L;

	/**
	 * Modifico el mensaje de la exception
	 * @param msg
	 */
	public OperatorInitException(String msg){
		super(msg);
	}
}
