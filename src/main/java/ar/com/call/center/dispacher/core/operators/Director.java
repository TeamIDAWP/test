package ar.com.call.center.dispacher.core.operators;

import ar.com.call.center.dispacher.core.exceptions.OperatorInitException;

public class Director extends ReceptorCall{
	
	public Director(String username, Integer priority) throws OperatorInitException{
		super(username, priority);
	}

}
