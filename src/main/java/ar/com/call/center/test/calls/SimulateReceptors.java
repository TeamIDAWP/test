package ar.com.call.center.test.calls;

import ar.com.call.center.dispacher.core.enums.ReceptorType;
import ar.com.call.center.dispacher.core.exceptions.DispacherException;
import ar.com.call.center.dispacher.core.exceptions.OperatorInitException;
import ar.com.call.center.dispacher.services.Services;
import ar.com.call.center.dispacher.services.dtos.ReceptorDTO;

public class SimulateReceptors {

	public static void loadReceptors() throws DispacherException, OperatorInitException{
		
		Services services = new Services();
		services.registerReceptor(new ReceptorDTO("estela_suarez", ReceptorType.OPERATOR));
		
		services.registerReceptor(new ReceptorDTO("juan_lopez", ReceptorType.OPERATOR));
		services.registerReceptor(new ReceptorDTO("maria_benitez", ReceptorType.OPERATOR));

		services.registerReceptor(new ReceptorDTO("ernesto_sabato", ReceptorType.OPERATOR));
		services.registerReceptor(new ReceptorDTO("analia_galan", ReceptorType.OPERATOR));
		
		services.registerReceptor(new ReceptorDTO("marisa_estevez", ReceptorType.SUPERVISOR));
		services.registerReceptor(new ReceptorDTO("mauro_diaz", ReceptorType.DIRECTOR));		
	}
}
