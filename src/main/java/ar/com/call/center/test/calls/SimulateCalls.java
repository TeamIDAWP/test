package ar.com.call.center.test.calls;

import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicLong;

import ar.com.call.center.dispacher.services.dtos.CallDTO;

public class SimulateCalls {

	private static AtomicLong counter = new AtomicLong();
	
	public static Collection<CallDTO> getCalls(){
				
		Collection<CallDTO> calls = new HashSet<CallDTO>();
		for (int i = 0; i < 20; i++) {
			calls.add(new CallDTO(counter.getAndIncrement()));
		}	
		return calls;
	}
}
