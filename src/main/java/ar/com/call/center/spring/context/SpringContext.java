package ar.com.call.center.spring.context;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringContext {

	public static ApplicationContext context;
	
	public static ApplicationContext getContext(){
		if(context==null){
			context = new ClassPathXmlApplicationContext("applicationContext.xml");
		}
		return context;
	}
}
